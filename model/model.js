var mongoose = require('./db');
var userSchema = new mongoose.Schema({
    name: String,
    pwd: String,
    img: String,
    tel: String,
    address: Array,
    lng: Number,
    lat: Number
})
var userModel = mongoose.model('user', userSchema, 'user');
var typeOneSchema = new mongoose.Schema({
    name: String,
    icon: String,
});
var typeOneModel = mongoose.model("typeOne", typeOneSchema, "typeOne");
//二级分类
var typeTwoSchema = new mongoose.Schema({
    name: String,
    typeOneId: {
        type: mongoose.Types.ObjectId,
        ref: "typeOne", //关联typeOne表
    },
    img:String,
});
var typeTwoModel = mongoose.model("typeTwo", typeTwoSchema, "typeTwo");
//书籍
// var carlist = new mongoose.Schema({
//     name: String, //书名
//     author: String, //作者
//     press: String, //出版社
//     isbn: String,
//     describe: String, //描述
//     location: String, //位置
//     pinxiang: String,
//     price: Number,
//     image: String,
//     ziti: {
//         default: false,
//         type: Boolean,
//     },
//     discount: Number, //折扣
//     is_exempt: {   //是否包邮
//         type: Boolean,
//         default: false,
//     },
//     userId: String, //用户ID
// });
// var carlistModel = mongoose.model("carlist", carlist, "carlist");

var BooksSchema = new mongoose.Schema({
    name: String, //书名
    author: String, //作者
    press: String, //出版社
    isbn: String, //ISBN
    describe: String, //图书描述
    location: String, //图书位置
    pinxiang: String, //图书品相
    price: Number, //图书原价格
    image: String, //图书图片
    ziti: {
        default: false,
        type: Boolean,
    }, //是否可自提
    discount: Number, //图书折扣
    typeTwoId: {
        type: mongoose.Types.ObjectId,
        ref: "typeTwo", //关联typeTwo表 二级分类
    },
    typeOneId: {
        type: mongoose.Types.ObjectId,
        ref: "typeOne", //关联typeOne表 一级分类
    },
    userId: String, //用户ID
    is_exempt: {   //是否包邮
        type: Boolean,
        default: false,
    },
    nowprice: Number, //图书现价
    freight:Number ,//运费



});
var BooksModel = mongoose.model("book", BooksSchema, "book");
//历史记录
var historySchema = new mongoose.Schema({
    name: String,
});
var historyModel = mongoose.model("history", historySchema, "history");

const friendSchema = new mongoose.Schema({
    uid: {   // 用户id
        type: mongoose.Types.ObjectId,
        ref: 'users'
    },
    friendId: Array,   // 好友id
})

const friendModel = mongoose.model('friend', friendSchema, 'friend')
//聊天
const usersSchema = new mongoose.Schema({
    picture: String,   //图片
    username: String,   // 用户名
    password: String,   // 密码
    friend: Array,
})
const usersModel = mongoose.model('users', usersSchema, 'users')
const messageSchema = new mongoose.Schema({
    date: String,
    uid: String,
    sid: String,
    formId: String,
    sendId: String,
    msg: String,
    formUsername: String,
    type: String,
    sendUsername: String,
    formImg: String,
    sendImg: String,
    new: {
        type: Boolean,
        default: true
    }
})

const messageModel = mongoose.model("message", messageSchema, 'message')

var carlist = new mongoose.Schema({
    name: String,//书名
    author: String,//作者
    press: String,//出版社
    isbn: String,
    describe: String,//描述
    location: String,//位置
    pinxiang: String,
    price: Number,
    image: String,
    ziti: {
        default: false,
        type: Boolean
    },
    discount: Number,//折扣
    is_exempt: {
        type: Boolean,
        default: false
    },
    userId: String, //用户ID
    checked: {
        type: Boolean,
        default: false
    },
    is_pay:{
        type: Boolean,
        default: false
    }
})
var carlistModel = mongoose.model('carlist', carlist, 'carlist');

const Review = new mongoose.Schema({
    userId: String,
    say: String,
    gid: {
        type: mongoose.Types.ObjectId,
        ref: 'goods'
    }
})
var reviewModel = mongoose.model('review', Review, 'review');
const comment = new mongoose.Schema({
    userId: String,
    rid: {
        type: mongoose.Types.ObjectId,
        ref: 'review'
    },
    said: String
})
var commentModel = mongoose.model('comment', comment, 'comment');

module.exports = { typeOneModel, userModel,typeTwoModel, BooksModel, historyModel,carlistModel,friendModel,messageModel,usersModel,reviewModel,commentModel };

